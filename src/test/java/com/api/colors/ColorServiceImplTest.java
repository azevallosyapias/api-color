package com.api.colors;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.api.colors.dao.ColorsDao;
import com.api.colors.dto.TColor;
import com.api.colors.entity.Color;
import com.api.colors.entity.PaginationResponse;
import com.api.colors.service.ColorServiceImpl;
import com.api.colors.util.Constants;
import com.api.colors.util.Constants.OBSERV;

@RunWith(MockitoJUnitRunner.class)
public class ColorServiceImplTest {

	@Mock
	ColorsDao colorDaoMock;

	@InjectMocks
	ColorServiceImpl colorService;

	List<TColor> lstColors;
	List<Color> lstColors2;
	PaginationResponse<Color> pageR;
	Page<TColor> pColor;

	@Before
	public void setUp() {
		lstColors = new ArrayList<TColor>();
		TColor c1 = new TColor(1L, "cerulean", 2000, "#98B2D1", "15-4020", Constants.STATUS.ACTIVO.getCodigo(), null,
				LocalDateTime.now(), "127.0.0.1", OBSERV.CREATE, "admin");
		TColor c2 = new TColor(1L, "fuchsia rose", 2001, "#C74375", "17-2031", Constants.STATUS.ACTIVO.getCodigo(),
				null, LocalDateTime.now(), "127.0.0.1", OBSERV.CREATE, "admin");
		TColor c3 = new TColor(1L, "true red", 2002, "#BF1932", "19-1664", Constants.STATUS.ACTIVO.getCodigo(), null,
				LocalDateTime.now(), "127.0.0.1", OBSERV.CREATE, "admin");
		TColor c4 = new TColor(1L, "aqua sky", 2003, "#BF1932", "14-4811", Constants.STATUS.ACTIVO.getCodigo(), null,
				LocalDateTime.now(), "127.0.0.1", OBSERV.CREATE, "admin");
		TColor c5 = new TColor(1L, "tigerlily", 2004, "#E2583E", "17-4811", Constants.STATUS.ACTIVO.getCodigo(), null,
				LocalDateTime.now(), "127.0.0.1", OBSERV.CREATE, "admin");
		lstColors.add(c1);
		lstColors.add(c2);
		lstColors.add(c3);
		lstColors.add(c4);
		lstColors.add(c5);

		pageR = new PaginationResponse<Color>();
		lstColors2 = new ArrayList<Color>();
		Color c12 = new Color(1L, "cerulean", "#98B2D1");
		Color c22 = new Color(2L, "fuchsia rose", "#C74375");
		Color c32 = new Color(3L, "true red", "#BF1932");
		Color c42 = new Color(4L, "aqua sky", "#7BC4C4");
		Color c52 = new Color(5L, "tigerlily", "#E2583E");
		lstColors2.add(c12);
		lstColors2.add(c22);
		lstColors2.add(c32);
		lstColors2.add(c42);
		lstColors2.add(c52);
		pageR.setContent(lstColors2);

		pColor = new PageImpl<TColor>(lstColors);
	}

	@Test
	public void findAllColors() {
		int page = 0;
		int size = 5;
		pageR.setSize(size);
		pageR.setPage(page);
		pageR.setTotalPages(1);
		pageR.setTotalElements(Long.parseLong(lstColors2.size() + ""));

		when(colorDaoMock.findAll(Mockito.any(Pageable.class)) ).thenReturn(pColor);
		PaginationResponse<Color> r = colorService.findAllColors(page, size);

		assertNotNull(r);
		assertTrue(r.getContent().size()>0);
		
		
	}

	@Test
	public void findColorTest() throws Exception {
		Long id = 1L;
		TColor c = lstColors.get(0);
		Optional<TColor> op = Optional.ofNullable(c);
		when(colorDaoMock.findById(id)).thenReturn(op);

		Color r = colorService.findColor(id);

		assertNotNull(r);
		assertTrue(r.getId().equals(c.getColorId()));
		assertTrue(r.getColor().equals(c.getColorCode()));
		assertTrue(r.getName().equals(c.getColorName()));
		assertTrue(r.getPantone_value().equals(c.getColorPantoneV()));
	}

	@Test
	public void findColorTest_NotFound() throws Exception {
		Long id = -1L;
		Optional<TColor> op = Optional.ofNullable(null);
		when(colorDaoMock.findById(id)).thenReturn(op);
		Color r = colorService.findColor(id);
		assertNull(r);
	}

	@Test
	public void createColorTest() throws Exception {
		Color c = new Color();
		c.setColor("#F1D8A1");
		c.setName("blue");
		c.setPantone_value("78-9854");
		c.setYear(2020);

		when(colorDaoMock.save(Mockito.any(TColor.class))).thenReturn(loadTC(c));
		c = colorService.create(c);

		assertNotNull(c.getId());
		assertTrue(c.getColor().equals("#F1D8A1"));
		assertTrue(c.getName().equals("blue"));
		assertTrue(c.getPantone_value().equals("78-9854"));
	}

	@Test
	public void setColorTest_AllValues() {
		TColor c = new TColor();
		c.setColorCode("#A1B2C3");
		c.setColorId(99L);
		c.setColorName("red");
		c.setColorPantoneV("87-8745");
		c.setColorYear(2020);

		Color r = colorService.setColor(c, true);

		assertTrue(c.getColorCode().equals(r.getColor()));
		assertTrue(c.getColorId().equals(r.getId()));
		assertTrue(c.getColorName().equals(r.getName()));

		assertTrue(c.getColorPantoneV().equals(r.getPantone_value()));
		assertEquals(c.getColorYear(), r.getYear());
	}

	@Test
	public void setColorTest_SpecificValues() {
		TColor c = new TColor();
		c.setColorCode("#A1B2C3");
		c.setColorId(99L);
		c.setColorName("red");
		c.setColorPantoneV("87-8745");

		Color r = colorService.setColor(c, false);

		assertTrue(c.getColorCode().equals(r.getColor()));
		assertTrue(c.getColorId().equals(r.getId()));
		assertTrue(c.getColorName().equals(r.getName()));

		assertNull(r.getPantone_value());
		assertEquals(c.getColorYear(), 0);
	}

	private TColor loadTC(Color c) {
		TColor r = new TColor();
		r.setAuditFecope(null);
		r.setAuditFecre(LocalDateTime.now());
		r.setAuditIP("127.0.0.1");
		r.setAuditObs(OBSERV.CREATE);
		r.setAuditUser("admin");
		r.setColorCode(c.getColor());
		r.setColorId(Long.parseLong(lstColors.size() + ""));
		r.setColorName(c.getName());
		r.setColorPantoneV(c.getPantone_value());
		r.setColorStatu(Constants.STATUS.ACTIVO.getCodigo());
		r.setColorYear(c.getYear());
		return r;
	}
}
