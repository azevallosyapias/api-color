package com.api.colors;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.api.colors.controller.ColorController;
import com.api.colors.entity.Color;
import com.api.colors.entity.PaginationResponse;
import com.api.colors.service.IColorService;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

@RunWith(SpringRunner.class)
@WebMvcTest(ColorController.class)
public class ColorControllerTest {

	private Logger log = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private MockMvc mvc;

	@MockBean
	IColorService colorService;

	private String urlListColors;
	private String urlCreateColor;
	private String urlColorByID;
	private String apiVersion;

	List<Color> lstColors;

	PaginationResponse<Color> pageColor = new PaginationResponse<Color>();

	Color colorC;

	@Before
	public void setUp() {
		pageColor = new PaginationResponse<Color>();
		apiVersion = "/api/v1/colors";

		urlListColors = apiVersion + "/colores";
		urlCreateColor = apiVersion + "/create";
		urlColorByID = apiVersion + "/colores/_id";

		lstColors = new ArrayList<Color>();
		Color c1 = new Color(1L, "cerulean", "#98B2D1");
		Color c2 = new Color(2L, "fuchsia rose", "#C74375");
		Color c3 = new Color(3L, "true red", "#BF1932");
		Color c4 = new Color(4L, "aqua sky", "#7BC4C4");
		Color c5 = new Color(5L, "tigerlily", "#E2583E");
		lstColors.add(c1);
		lstColors.add(c2);
		lstColors.add(c3);
		lstColors.add(c4);
		lstColors.add(c5);
		pageColor.setContent(lstColors);

		colorC = new Color(20L, "blue", 2020, "#00FF00", "45-4587");
	}

	@Test
	public void getListColors_Status() throws Exception {
		int page = 0;
		int size = 5;
		pageColor.setSize(size);
		pageColor.setPage(page);
		pageColor.setTotalPages(1);
		pageColor.setTotalElements(Long.parseLong(lstColors.size() + ""));
		when(colorService.findAllColors(0, 5)).thenReturn(pageColor);

		mvc.perform(get(urlListColors).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	public void getListColors_Data() throws Exception {
		int page = 0;
		int size = 5;
		pageColor.setSize(size);
		pageColor.setPage(page);
		pageColor.setTotalPages(1);
		pageColor.setTotalElements(Long.parseLong(lstColors.size() + ""));
		String jsonRpta = "{\"page\":0,\"size\":5,\"content\":[{\"id\":1,\"name\":\"cerulean\",\"color\":\"#98B2D1\"},{\"id\":2,\"name\":\"fuchsia rose\",\"color\":\"#C74375\"},{\"id\":3,\"name\":\"true red\",\"color\":\"#BF1932\"},{\"id\":4,\"name\":\"aqua sky\",\"color\":\"#7BC4C4\"},{\"id\":5,\"name\":\"tigerlily\",\"color\":\"#E2583E\"}],\"totalPages\":1,\"totalElements\":5}";

		when(colorService.findAllColors(Mockito.anyInt(), Mockito.anyInt())).thenReturn(pageColor);
		MvcResult result = mvc.perform(get(urlListColors).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String text = result.getResponse().getContentAsString();
		JsonElement r = JsonParser.parseString(text);
		log.info("response: " + text);
		assertTrue(r.getAsJsonObject().get("content").isJsonArray());
		assertEquals(r, JsonParser.parseString(jsonRpta));
	}

	@Test
	public void createColor_Status() throws Exception {
		String requestJson = "{\"color\": \"#00FF00\",\"name\": \"blue\",\"pantone_value\": \"45-4587\",\"year\": 2020}";
		when(colorService.create(Mockito.any(Color.class))).thenReturn(colorC);
		mvc.perform(post(urlCreateColor).accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(requestJson)).andExpect(status().isCreated())
				.andDo(mvcR -> {
					String json = mvcR.getResponse().getContentAsString();
					log.info("Created: " + json);
				});

	}

	@Test
	public void getColorByID_Status() throws Exception {
		Long idColor = 1L;
		urlColorByID = urlColorByID.replaceAll("_id", idColor.toString());
		when(colorService.findColor(Mockito.anyLong())).thenReturn(lstColors.get(0));

		mvc.perform(get(urlColorByID).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	public void getColorByID_Status400() throws Exception {
		urlColorByID = urlColorByID.replaceAll("_id", "a");
		mvc.perform(get(urlColorByID).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void getColorByID_Status404() throws Exception {
		Long idColor = 1L;
		urlColorByID = urlColorByID.replaceAll("_id", idColor.toString());
		when(colorService.findColor(idColor)).thenReturn(null);
		mvc.perform(get(urlColorByID).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());

	}

	@Test
	public void getColorByID_DataJson() throws Exception {
		String jsonRpta = "{\"id\":20,\"name\":\"blue\",\"year\":2020,\"color\":\"#00FF00\",\"pantone_value\":\"45-4587\"}";
		Long idColor = 1L;
		urlColorByID = urlColorByID.replaceAll("_id", idColor.toString());
		
		when(colorService.findColor(Mockito.anyLong())).thenReturn(colorC);
		MvcResult result = mvc.perform(get(urlColorByID)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();
		
		String text=result.getResponse().getContentAsString();
		JsonElement r = JsonParser.parseString(text);
		assertTrue(r.isJsonObject());
		assertEquals(r, JsonParser.parseString(jsonRpta));
	}

	@Test
	public void getColorByID_XML() throws Exception {
		String xmlRpta = "<Color><id>20</id><name>blue</name><year>2020</year><color>#00FF00</color><pantone_value>45-4587</pantone_value></Color>";
		Long idColor = 1L;
		urlColorByID = urlColorByID.replaceAll("_id", idColor.toString())+"?mediaType=xml";
		
		when(colorService.findColor(Mockito.anyLong())).thenReturn(colorC);
		MvcResult result = mvc.perform(get(urlColorByID).header("Accept", MediaType.APPLICATION_XML_VALUE)
				.contentType(MediaType.APPLICATION_XML))
				.andExpect(status().isOk())
				.andReturn();
		
		String text=result.getResponse().getContentAsString();
		log.info("Response XML: "+text);
		assertEquals(text, xmlRpta);
	}
}
