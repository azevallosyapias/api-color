package com.api.colors.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.api.colors.dto.TColor;

public interface ColorsDao extends PagingAndSortingRepository<TColor, Long> {

	List<TColor> findByColorStatu(double price, Pageable pageable);

}
