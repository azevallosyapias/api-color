package com.api.colors.entity;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PaginationResponse<T> {

	@Getter
	@Setter
	Integer page;

	@Getter
	@Setter
	Integer size;

	@Getter
	@Setter
	List<T> content;
	
	@Getter
	@Setter
	Integer totalPages;
	
	@Getter
	@Setter
	Long totalElements;
}
