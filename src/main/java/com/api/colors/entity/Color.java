package com.api.colors.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class Color {

	@Getter
	@Setter
	private Long id;

	@Getter
	@Setter
	private String name;

	@Getter
	@Setter
	private int year;

	@Getter
	@Setter
	private String color;

	@Getter
	@Setter
	private String pantone_value;

	public Color(Long id, String name, String color) {
		this.id = id;
		this.name = name;
		this.color = color;
	}

}
