package com.api.colors.service;

import com.api.colors.entity.Color;
import com.api.colors.entity.PaginationResponse;

public interface IColorService {

	PaginationResponse<Color> findAllColors(int page, int size);
	Color findColor(Long id);
	Color create(Color color);
	
}
