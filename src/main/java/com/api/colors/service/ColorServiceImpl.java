package com.api.colors.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.api.colors.dao.ColorsDao;
import com.api.colors.dto.TColor;
import com.api.colors.entity.Color;
import com.api.colors.entity.PaginationResponse;
import com.api.colors.util.Constants;
import com.api.colors.util.Constants.OBSERV;

@Service
public class ColorServiceImpl implements IColorService {

	@Autowired
	ColorsDao colorDao;

	/**
	 * <p> Metodo para listar de forma paginada los Colores
	 * </p>
	 * @author Alonso
	 * @param int page, int size
	 * @return La lista paginada de colores que cumplan con el filtro de paginacion
	 */
	@Override
	public PaginationResponse<Color> findAllColors(int page, int size) {
		if (size == -1) {
			size = Integer.MAX_VALUE;
		}
		Pageable filterP = PageRequest.of(page, size);
		Page<TColor> allC = colorDao.findAll(filterP);

		List<Color> list2 = allC.get().map(c -> setColor(c, false)).collect(Collectors.toList());

		PaginationResponse<Color> rpta = new PaginationResponse<>();
		rpta.setContent(list2);
		rpta.setPage(page);
		rpta.setSize(size);
		rpta.setTotalElements(allC.getTotalElements());
		rpta.setTotalPages(allC.getTotalPages());

		return rpta;
	}

	/**
	 * <p> Metodo para obtener el Color por Id
	 * </p>
	 * @author Alonso
	 * @param Long id
	 * @return El Color registrado que tenga el Id 
	 */
	@Override
	public Color findColor(Long id) {
		Color c = null;
		Optional<TColor> op = colorDao.findById(id);
		if (op.isPresent()) {
			c = setColor(op.get(), true);
		}
		return c;
	}

	/**
	 * <p> Metodo para registrar el Color
	 * </p>
	 * @author Alonso
	 * @param Object Color
	 * @return El Color registrado
	 */
	@Override
	public Color create(Color color) {
		TColor c = new TColor();
		c.setColorYear(color.getYear());
		c.setColorStatu(Constants.STATUS.ACTIVO.getCodigo());
		c.setColorPantoneV(color.getPantone_value());
		c.setColorName(color.getName());
		c.setColorCode(color.getColor());
		c.setAuditUser("admin");
		c.setAuditObs(OBSERV.CREATE);
		c.setAuditIP("127.0.0.1");
		c.setAuditFecre(LocalDateTime.now());
		c.setAuditFecope(null);
		
		c= colorDao.save(c);
		
		color=setColor(c, true);
		return color;
	}
	
	/**
	 * <p> Metodo para homologar los valores de la entidad de persistencia(TColor) con la entidad a retornar por los controladores (Color)
	 * </p>
	 * @author Alonso
	 * @param Entidad TColor
	 * @return Entidad Color
	 */
	public Color setColor(TColor t, boolean campos) {
		Color c = new Color();
		c.setColor(t.getColorCode());
		c.setId(t.getColorId());
		c.setName(t.getColorName());
		if (campos) {
			c.setPantone_value(t.getColorPantoneV());
			c.setYear(t.getColorYear());
		}

		return c;
	}
}
