package com.api.colors.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
@Aspect
public class Aspects {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Pointcut("within(com.api.colors.controller.*)")
	public void methodInController() {

	}
	
	@Before("methodInController()")
	public void advise1(JoinPoint jp) {
		logger.info(">> Begin >>  Method: " + jp.getSignature().getName());
		for (Object arg : jp.getArgs()) {
			logger.info(">>"+String.format(" Args: %s", arg));
		}
	}
	
	@AfterReturning(pointcut = "methodInController()", returning = "returnValue")
	public void adviceAfter(JoinPoint jp, Object returnValue) {

		logger.info("<< Return <<  Method: " + jp.getSignature().getName());
		if (returnValue != null) {
			try {
				logger.info("<<"+new ObjectMapper().writeValueAsString(returnValue));
			} catch (JsonProcessingException e) {
				logger.info("<<"+returnValue.toString());
			}
		}
	}
	
	@AfterThrowing(pointcut = "methodInController()", throwing = "exception1")
	public void adviceAfterThrow(JoinPoint jp, Exception exception1) {
		logger.info("<< Exception -> " + exception1.getMessage());
		logger.info("<< Return <<  Method: " + jp.getSignature().getName());
	}
	
	@Around("within(com.api.colors.service.*)")
	public Object postTrace(ProceedingJoinPoint jp) throws Throwable {
		String pref = "Service: " + jp.getSignature().getName();
		logger.info("-- "+pref);
		for (Object arg : jp.getArgs()) {
			logger.info("-- "+String.format(pref + " Args: %s", arg));
		}
		Object o = jp.proceed();
		try {
			logger.info("-- "+pref + " Result : " + new ObjectMapper().writeValueAsString(o));
		} catch (Exception e) {
			// TODO: handle exception
		}
		logger.info("-- "+pref);
		return o;
	}
}
