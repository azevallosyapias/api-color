package com.api.colors.controller;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.api.colors.entity.Color;
import com.api.colors.entity.PaginationResponse;
import com.api.colors.exception.ColorNotFoundException;
import com.api.colors.service.IColorService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/v1/colors")
@Api(tags = "Colors")
public class ColorController {

	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	IColorService colorService;
	
	/**
	 * <p> Metodo para retornar la lista de Colores con Paginacion de la BD
	 * </p>
	 * @author Alonso
	 * @param int page, int size
	 * @return La lista de Colores con paginacion (page, size, content (lista de los registros), totalPages, totalElements)
	 */
	@ApiOperation(value = "Get all colors", notes = "Get all colors")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_OK, message = "OK") })
	@GetMapping(path = "/colores", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	@ResponseStatus(HttpStatus.OK)
	public PaginationResponse<Color> getListColors(@RequestParam(defaultValue = "0") int page,
	        @RequestParam(defaultValue = "-1") int size){
		log.info("get page of colors");
		return colorService.findAllColors(page,size);
	}
	
	/**
	 * <p> Metodo para obtener un Color por Id
	 * </p>
	 * @author Alonso
	 * @param id : Long Id del Color
	 * @return El color registrado con el Id enviado
	 */
	@ApiOperation(value = " ", notes = "Get one Color")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_OK, message = "OK"), 
					@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message ="Invalid color id"), 
					@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message ="Color not found") 
	})
	@GetMapping(path = "/colores/{id}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	@ResponseStatus(HttpStatus.OK)
	public Color getUserByID(@PathVariable("id") Long id) {
		log.info("GetMapping - value: /colores/"+id);
		Color color=colorService.findColor(id);
		if(null==color) {
			throw new ColorNotFoundException("User not found");
		}
		return color;
	}
	
	/**
	 * <p> Metodo para registrar un Color
	 * </p>
	 * @author Alonso
	 * @param Objeto Color
	 * @return El color registrado
	 */
	@ApiOperation(value = "Create Color", notes = "")
	@ApiResponses({
		@ApiResponse(code = HttpServletResponse.SC_CREATED, message ="CREATED"), 
		@ApiResponse(code = HttpServletResponse.SC_METHOD_NOT_ALLOWED, message ="Invalid input") 
	})
	@PostMapping(path = "/create", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public Color createUser(@RequestBody Color color) {
		log.info("PostMapping - value: /create "+color.toString());
		return colorService.create(color);
	}
}
