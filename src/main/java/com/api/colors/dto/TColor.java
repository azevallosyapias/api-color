package com.api.colors.dto;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.api.colors.util.Constants.OBSERV;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "t_color")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TColor {

	@Id
	@SequenceGenerator(name = "sequence_colors", sequenceName = "sq_colors", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_colors")
	@Column(name = "col_iident")
	@Setter 
	@Getter
	private Long colorId;
	
	@Getter
	@Setter
	@Column(name = "col_name")
	private String colorName;
	
	@Getter
	@Setter
	@Column(name = "col_year")
	private int colorYear;
	
	@Getter
	@Setter
	@Column(name = "col_color")
	private String colorCode;
	
	@Getter
	@Setter
	@Column(name = "col_pantone_value")
	private String colorPantoneV;
	
	@Getter
	@Setter
	@Column(name = "col_cstatu")
	private String colorStatu;
	
	@Column(name = "audit_dfecope")
	@Setter 
	@Getter
	private LocalDateTime auditFecope;
	
	@Column(name = "audit_dfecre")
	@Setter 
	@Getter
	private LocalDateTime auditFecre;
	
	@Column(name = "audit_vipad")
	@Setter 
	@Getter
	private String auditIP;
	
	@Column(name = "audit_vobs")
	@Setter 
	@Getter
	@Enumerated(EnumType.STRING)
	private OBSERV auditObs;
	
	@Column(name = "audit_vuser")
	@Setter 
	@Getter
	private String auditUser;
	
}
