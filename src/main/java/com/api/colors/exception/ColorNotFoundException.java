package com.api.colors.exception;

public class ColorNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ColorNotFoundException() {
		super();
	}

	public ColorNotFoundException(String message) {
		
		super(message);
	}

	public ColorNotFoundException(String message, Throwable cause) {
		super(message);
	}
}
