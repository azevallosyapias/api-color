package com.api.colors.exception;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalRestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler({ ColorNotFoundException.class })
	public ResponseEntity<ApiErrorResponse> customerNotFound(ColorNotFoundException ex, WebRequest request) {
		ApiErrorResponse apiResponse = new ApiErrorResponse.ApiErrorResponseBuilder()
				.withDetail("Not a valid color id.Please provide a valid color id or contact system admin.")
				.withMessage("User not found").withError_code("404").withStatus(HttpStatus.NOT_FOUND)
				.atTime(LocalDateTime.now(ZoneOffset.UTC)).build();
		return new ResponseEntity<ApiErrorResponse>(apiResponse, HttpStatus.NOT_FOUND);
	}
}
