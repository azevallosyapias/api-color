-- Sequence: public.sq_colors

-- DROP SEQUENCE public.sq_colors;

CREATE SEQUENCE public.sq_colors
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.sq_colors
  OWNER TO postgres;


-- Table: public.t_color

-- DROP TABLE public.t_color;

CREATE TABLE public.t_color
(
  col_iident bigint NOT NULL, -- Id de tabla
  col_name character varying(255), -- Nombre del color
  col_year character varying(4), -- Anio
  col_color character varying(10), -- Codigo de color
  col_pantone_value character varying(10), -- Pantone Value
  col_cstatu character(1), -- Estado del registro
  audit_dfecope timestamp without time zone, -- Campo de auditoria - fecha de actualizacion del registro
  audit_dfecre timestamp without time zone, -- Campo de auditoria - fecha de creacion del registro
  audit_vipad character varying(255), -- Campo de auditoria - ip origen de la peticion
  audit_vobs character varying(255), -- Campo de auditoria - observacion
  audit_vuser character varying(255), -- Campo de auditoria - usuario
  CONSTRAINT pk_pet_id PRIMARY KEY (col_iident)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.t_color
  OWNER TO postgres;
COMMENT ON COLUMN public.t_color.col_iident IS 'Id de tabla';
COMMENT ON COLUMN public.t_color.col_name IS 'Nombre del color';
COMMENT ON COLUMN public.t_color.col_year IS 'Anio';
COMMENT ON COLUMN public.t_color.col_color IS 'Codigo de Color';
COMMENT ON COLUMN public.t_color.col_pantone_value IS 'Pantone Value';
COMMENT ON COLUMN public.t_color.col_cstatu IS 'Estado del registro';

COMMENT ON COLUMN public.t_color.audit_dfecope IS 'Campo de auditoria - fecha de actualizacion del registro';
COMMENT ON COLUMN public.t_color.audit_dfecre IS 'Campo de auditoria - fecha de creacion del registro';
COMMENT ON COLUMN public.t_color.audit_vipad IS 'Campo de auditoria - ip origen de la peticion';
COMMENT ON COLUMN public.t_color.audit_vobs IS 'Campo de auditoria - observacion ';
COMMENT ON COLUMN public.t_color.audit_vuser IS 'Campo de auditoria - usuario';


-- load data initial

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'cerulean', 2000, '#98B2D1', '15-4020', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'fuchsia rose', 2001, '#C74375', '17-2031', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'true red', 2002, '#BF1932', '19-1664', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'aqua sky', 2003, '#7BC4C4', '14-4811', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'tigerlily', 2004, '#E2583E', '17-1456', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'blue turquoise', 2005, '#53B0AE', '15-5217', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'sand dollar', 2006, '#DECDBE', '13-1106', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'chili pepper', 2007, '#9B1B30', '19-1557', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'blue iris', 2008, '#5A5B9F', '18-3943', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'mimosa', 2009, '#F0C05A', '14-0848', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'turquoise', 2010, '#45B5AA', '15-5519', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'honeysuckle', 2011, '#D94F70', '18-2120', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');
