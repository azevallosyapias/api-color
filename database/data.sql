
INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'cerulean', 2000, '#98B2D1', '15-4020', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'fuchsia rose', 2001, '#C74375', '17-2031', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'true red', 2002, '#BF1932', '19-1664', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'aqua sky', 2003, '#7BC4C4', '14-4811', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'tigerlily', 2004, '#E2583E', '17-1456', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'blue turquoise', 2005, '#53B0AE', '15-5217', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'sand dollar', 2006, '#DECDBE', '13-1106', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'chili pepper', 2007, '#9B1B30', '19-1557', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'blue iris', 2008, '#5A5B9F', '18-3943', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'mimosa', 2009, '#F0C05A', '14-0848', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'turquoise', 2010, '#45B5AA', '15-5519', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');

INSERT INTO public.t_color
(col_iident, col_name, col_year, col_color, col_pantone_value, col_cstatu, audit_dfecope, audit_dfecre, audit_vipad, audit_vobs, audit_vuser)
VALUES (nextval('sq_colors'), 'honeysuckle', 2011, '#D94F70', '18-2120', 'A', null, now(), '127.0.0.1', 'CREATE', 'admin');