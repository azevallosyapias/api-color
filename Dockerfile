FROM maven:3.6.3-jdk-11-slim AS builder
WORKDIR /apiColors
COPY pom.xml .
RUN mvn -B -e -C -T 1C org.apache.maven.plugins:maven-dependency-plugin:3.0.2:go-offline
COPY src ./src
RUN ls -la ./src
RUN mvn clean package
RUN mvn jacoco:prepare-agent test jacoco:report
RUN ls -la /apiColors


FROM openjdk:11.0.6-jdk
LABEL maintainer="alonsofisi@gmail.com"
WORKDIR /workspace
RUN ls -la /workspace
COPY --from=builder /apiColors/target/api*color*.jar app.jar
RUN ls -la /workspace
ENTRYPOINT exec java -jar /workspace/app.jar
EXPOSE 7071

