# Project API Colors

Proyecto Backend para el manejo de Colores a utilizar en la empresa

_Se encuentra desplegado en AWS en la siguiente ruta:_

```
Swagger del API: http://3.129.65.21:7071/swagger-ui.html#/
```

_Para poder utilizar el API, se adjunta el File exportado de POSTMAN y se encuentra en la siguiente ruta:_

```
api-colors/api-colors.postman_collection.json
```

## Comenzando 🚀

_Para obtener una copia del repositorio solo se necesitaría clonar con el siguiente comando: ._

```
git clone https://azevallosyapias@bitbucket.org/azevallosyapias/api-color.git
```

### Pre-requisitos 📋

_Para poder desplegar el proyecto es necesario tener instalado:_

```
- Maven
- Java version 8 o superior
- Docker

```

### Despliegue 🔧

_Para poder iniciar todos los contenedores basta con ejecutar el comando docker-compose_

_Ubicarse en la ruta del file docker-compose.yml (api-colors/docker-compose.yml) y ejecutar:_

```
docker-compose up -d
```
_Este comando iniciara todos los contenedores necesarios para deployar el API_

_Un ejemplo de salida del comando_

```
Creating api-colors_bdpostgres_1 ... done
Creating api-colors_api-color_1  ... done
```

_Una vez iniciado los contenedores se habilita la siguiente ruta_

```
http://3.129.65.21:7071/swagger-ui.html#/

Endpoint disponibles:

Lista de colores:
	uri: http://3.129.65.21:7071/api/v1/colors/colores?page=0&size=5&mediaType=xml
	parametros:
		page: pagina solicitada
		size: cantidad de registros por pagina
		mediaType: para obtener la respuesta en xml o json 
			Para obtener el response en xml-> mediaType=xml
			Para obtener el response en json-> mediaType=json
			En caso no se envie el parametros por default retornara en formato json
			
Obtener Color por ID:
	uri: http://3.129.65.21:7071/api/v1/colors/colores/1?mediaType=json
	parametros:
		Id: Id a buscar
		mediaType: para obtener la respuesta en xml o json 
			Para obtener el response en xml-> mediaType=xml
			Para obtener el response en json-> mediaType=json
			En caso no se envie el parametros por default retornara en formato json
			
Registrar Color:
	uri: http://3.129.65.21:7071/api/v1/colors/create
			
```


_Nota: Los contenedores utilizados son los siguientes:_

```
https://hub.docker.com/repository/docker/alonsozy/database-colors
https://hub.docker.com/repository/docker/alonsozy/api-color
```

## Ejecutando las pruebas ⚙️

_Para ejecutar los UnitTest ejecutar el siguiente comando en la ubicacion del pom.xml_

```
mvn test
```

_Para poder ver la cobertura del código utilizando jacoco, ejecutar lo siguiente:_

```
mvn jacoco:prepare-agent test jacoco:report
```

_Este comando analizará y nos dispondrá un html con la cobertura_

_Este file se ubicará en la siguiente ruta:_

```
api-colors\target\site\jacoco\index.html
```
_Se coberturó un 88% con las pruebas_

## Construido con 🛠️

_Herramientas utilizadas en el desarrollo del proyecto_

```
* Java 8
* Spring Framework
* Spring Boot
* Spring MVC
* Spring JPA
* AOP - para el manejo de Logs
* Swagger: utilizado para la documentacion del contrato del API
* JUnit
* Mockito
* Jacoco: utilizado para la revision y visualizacion de la cobertura del codigo
* JavaDocs: utilizado para documentar el codigo
* BD PostgreSQL 9.5
* Docker
* Docker Compose
* Postman para las pruebas
```


## Autor ✒️

* **Marino Alonso Z.Y. **

_Links:_
* [Linkedin](https://www.linkedin.com/in/marino-alonso-zevallos-yapias-82a200196/)
* Bitbucket : [azevallosyapias](https://bitbucket.org/azevallosyapias/)
* GitHub: [alonsozy](https://github.com/alonsozy)
* DockerHub: [alonsozy](https://hub.docker.com/u/alonsozy)



